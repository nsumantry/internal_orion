<?php
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

function send_mail_confirmation(SmtpOptions $options,$email,$token,$idPelanggan){
	try {
		$transport = new SmtpTransport();
		
		$transport->setOptions($options);
		
		$mail = new Mail\Message();
		$str = "<h1>E-Tiket Confirmation Mail.</h1>";
		$str .= "<p>this is confirmation email, please follow link below to confirm your email</p>";
		$str .= "<button onclick=\"window.location.href='http://etiketpulauseribudishub.com/e_tiket_api/public/token_aktivasi/aktivasi?token=$token&id_pelanggan=$idPelanggan';\">Confirm Email</button>"; 
		$str .= "<p>if link above doesn't work, please copy link below and paste on browser address bar to confirm email</p>";
		$str .= "<p>http://etiketpulauseribudishub.com/e_tiket_api/public/token_aktivasi/aktivasi?token=$token&id_pelanggan=$idPelanggan</p>";
		
		$html = new MimePart($str);
		$html->type = "text/html";
		$body = new MimeMessage();
		$body->setParts(array($html));
	
		$mail->setBody($body);
		$mail->setFrom('noreply@etiketpulauseribudishub.com', 'E-Tiket Pulau Seribu Dishub');
		// $mail->addTo('mmrsgsw@gmail.com', 'Matthew');
		$mail->addTo($email);
		$mail->setSubject('E-Tiket Confirmation Mail.');

		$transport->send($mail);
	} catch(Exception $e) {
		return false;
	}
	return true;
}
function send_mail_reset_link(SmtpOptions $options,$email,$token,$idPelanggan){
	try {
		$transport = new SmtpTransport();
		
		$transport->setOptions($options);
		
		$mail = new Mail\Message();
		$str = "<h1>E-Tiket - Password Reset.</h1>";
		$str .= "<p>this is Passworde Reset confirmation email, please follow link below to reset your password</p>";
		$str .= "<button onclick=\"window.location.href='http://etiketpulauseribudishub.com/reset_password_form/$token/$idPelanggan';\">Reset Password</button>"; 
		$str .= "<p>if link above doesn't work, please copy link below and paste on browser address bar to confirm email</p>";
		$str .= "<p>http://etiketpulauseribudishub.com/reset_password_form/$token/$idPelanggan</p>";
		
		$html = new MimePart($str);
		$html->type = "text/html";
		$body = new MimeMessage();
		$body->setParts(array($html));
	
		$mail->setBody($body);
		$mail->setFrom('noreply@etiketpulauseribudishub.com', 'E-Tiket Pulau Seribu Dishub');
		// $mail->addTo('mmrsgsw@gmail.com', 'Matthew');
		$mail->addTo($email);
		$mail->setSubject('E-Tiket Password Reset.');

		$transport->send($mail);
	} catch(Exception $e) {
		return false;
	}
	return true;
}
function send_mail_bayar_confirmed(SmtpOptions $options,$email,$nomorPesanan,$idPesanan){
	try {
		$transport = new SmtpTransport();
		
		$transport->setOptions($options);
		
        $mail = new Mail\Message();
        
		$str = "<h1>E-Tiket - Pembayaran berhasil.</h1>";
		$str .= "<p>pembayaran pesanan anda dengan nomor $nomorPesanan berhasil. silahkan login ke aplikasi untuk cetak tiket anda</p>";
		// $str .= "<button onclick=\"window.location.href='http://etiketpulauseribudishub.com/reset_password_form/$token/$idPelanggan';\">Reset Password</button>"; 
		// $str .= "<p>if link above doesn't work, please copy link below and paste on browser address bar to confirm email</p>";
		// $str .= "<p>http://etiketpulauseribudishub.com/reset_password_form/$token/$idPelanggan</p>";
		
		$html = new MimePart($str);
		$html->type = "text/html";
		$body = new MimeMessage();
		$body->setParts(array($html));
	
		$mail->setBody($body);
		$mail->setFrom('noreply@etiketpulauseribudishub.com', 'E-Tiket Pulau Seribu Dishub');
		// $mail->addTo('mmrsgsw@gmail.com', 'Matthew');
		$mail->addTo($email);
		$mail->setSubject('E-Tiket - Pembayaran berhasil.');

		$transport->send($mail);
	} catch(Exception $e) {
		return false;
	}
	return true;
}

function send_mail_html(SmtpOptions $options,$email,$subject,$str){
	try {
		$transport = new SmtpTransport();
		
		$transport->setOptions($options);
		
		$mail = new Mail\Message();
		
		$html = new MimePart($str);
		$html->type = "text/html";
		$body = new MimeMessage();
		$body->setParts(array($html));
	
		$mail->setBody($body);
		$mail->setFrom('noreply@etiketpulauseribudishub.com', 'E-Tiket Pulau Seribu Dishub');
		// $mail->addTo('mmrsgsw@gmail.com', 'Matthew');
		$mail->addTo($email);
		$mail->setSubject($subject);

		$transport->send($mail);
	} catch(Exception $e) {
		return false;
	}
	return true;
}