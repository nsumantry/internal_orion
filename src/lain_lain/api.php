<?php
use Slim\Http\Request;
use Slim\Http\Response;

$cekAPIKey = function($request, $response, $next){

    $key = $request->getQueryParam("key");    

    if(!isset($key)){
        return $response->withJson(["status" => "API Key required"]);
    }
    
    $sql = "SELECT * FROM api_users WHERE api_key=:api_key and expired_date > now()";
    $stmt = $this->db->prepare($sql);
    $stmt->execute([":api_key" => $key]);
    
    if($stmt->rowCount() > 0){
        $result = $stmt->fetch();
        if($key == $result["api_key"]){
        
            $stmt->execute([":api_key" => $key]);
            
            return $response = $next($request, $response);
        }
    }

    return $response->withJson(["status" => "Unauthorized"]);
};

$cekAPIKeyArray = function($request, $response, $next){

    $key = $request->getQueryParam("key");    

    if(!isset($key)){
        return $response->withJson(array(["status" => "API Key required"]));
    }
    
    $sql = "SELECT * FROM api_users WHERE api_key=:api_key and expired_date > now()";
    $stmt = $this->db->prepare($sql);
    $stmt->execute([":api_key" => $key]);
    
    if($stmt->rowCount() > 0){
        $result = $stmt->fetch();
        if($key == $result["api_key"]){
        
            $stmt->execute([":api_key" => $key]);
            
            return $response = $next($request, $response);
        }
    }

    return $response->withJson(array(["status" => "Unauthorized"]));
};

$app->get('/api/cek', function (Request $request, Response $response) {
    return $response->withJson(["status" => "Authorized"]);        
})->add($cekAPIKey);

function RandomToken($length = 32){
    if(!isset($length) || intval($length) <= 8 ){
      $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    } 
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

