<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';

$app->get('/master_project/load', function (Request $request, Response $response) {
	$db = $this->db;
	$cari        = $request->getQueryParam("cari");
	$orderBy  	 = $request->getQueryParam("order_by");
	$limit    	 = $request->getQueryParam("limit");
	$offset   	 = $request->getQueryParam("offset");

	$filter  = "";
	$filter2 = "";

	if (!empty($cari)){
		$filter .= " AND ((nama LIKE '%$cari%')) ";
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

	$data = [];	
	try {

		$sql = 	"SELECT seq, nama, disable_date ".
                "FROM master_project ".
				"WHERE seq <> 0 $filter $filter2 ";

		$query  = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil  = [];		
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil); 
});