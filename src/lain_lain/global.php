<?php
  use Slim\Http\Request;
  use Slim\Http\Response;

  $app->post("/global/sqltonamevaluelist", function (Request $request, Response $response, array $args) {       
    $data = $request->getParsedBody();
    $sql   = $data["sql"];
    $query = $this->db->prepare($sql);
    $result = $query->execute();
    if ($result) {
      if ($query->rowCount()) {
        $data = $query->fetchAll();
      }else{
        $data = array(
          'kode' => 2,
          'keterangan' => 'Tidak ada data',
          'data' => null);
      }
    }else{
      $data = array(
        'kode' => 100,
        'keterangan' => 'Terdapat error',
        'data' => null);
    }
    return $response->withJson($data);
  });


  $app->post("/global/getvalueLapKontrolData", function (Request $request, Response $response, array $args) {       
    $data = $request->getParsedBody();
    $nama_db       = $data['nama_db'];
    $tbl_blm_jalan = $data['tbl_blm_jalan'];
    $tbl_blm_ambil = $data['tbl_blm_ambil'];
    $par_blm_ambil = $data['par_blm_ambil'];
    $tbl_dml_error = $data['tbl_dml_error'];
    $tbl_min_tgl   = $data['tbl_min_tgl'];
    $field_min_tgl = $data['field_min_tgl'];

    if(!empty($tbl_blm_jalan)) {
      $query = $this->db->prepare("select count(*) as blm_jalan from $nama_db.$tbl_blm_jalan");
      $result = $query->execute();
      if ($result) {
         $hasil = $query->fetch();
         $blm_jalan = $hasil["blm_jalan"];
      }
    }else{
      $blm_jalan = '-';     
    }

    if(!empty($tbl_blm_ambil)) {
      $query1 = $this->db->prepare("select count(*) as blm_ambil from $nama_db.$tbl_blm_ambil where $par_blm_ambil");
      $result1 = $query1->execute();
      if ($result1) {
         $hasil = $query1->fetch();
         $blm_ambil = $hasil["blm_ambil"];
      }
    }else{
      $blm_ambil = '-';     
    }

    if(!empty($tbl_dml_error)) {
      $query2 = $this->db->prepare("select count(*) as dml_error from $nama_db.$tbl_dml_error");
      $result2 = $query2->execute();
      if ($result2) {
         $hasil = $query2->fetch();
         $dml_error = $hasil["dml_error"];
      }
    }else{
      $dml_error = '-';     
    }

    if(!empty($tbl_min_tgl)) {
      $query3 = $this->db->prepare("select min($field_min_tgl) as min_tgl from $nama_db.$tbl_min_tgl");
      $result3 = $query3->execute();
      if ($result3) {
         $hasil = $query3->fetch();
         $min_tgl_temp = $hasil["min_tgl"];
         $timestamp = strtotime($min_tgl_temp);
         $min_tgl = date("d-m-Y", $timestamp);
      }
    }else{
      $min_tgl = '-';     
    }

    $data = array(
        'blm_jalan' => $blm_jalan,
        'blm_ambil' => $blm_ambil,
        'dml_error' => $dml_error,
        'min_tgl' => $min_tgl);

    if (empty($data)) {
      $data = array(
        'kode' => 100,
        'keterangan' => 'Data Kosong',
        'data' => null);
    }

    return $response->withJson($data);
  });


  $app->post("/global/getintegerfromsql", function (Request $request, Response $response, array $args) {       
    $data = $request->getParsedBody();
    $sql   = $data["sql"];
    $db = $this->db;
    $query = $db->prepare($sql);

  
    $result = $query->execute();  
    if ($result) {
          if ($query->rowCount()) {
              $data = $query->fetch();               
              $hasil = $data["hasil"];            
          }
    }
    $data = array(
          'hasil' => "$hasil");

    return $response->withJson($data); 
  });



?>
