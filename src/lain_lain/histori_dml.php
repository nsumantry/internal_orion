<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';

$app->post('/histori_dml/insert', function (Request $request, Response $response) {
    $dml = $request->getParsedBody();

	$uploadedFiles = $request->getUploadedFiles();		
	
	$uploadedFile = $uploadedFiles['file'];
	if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
		$extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
		// ubah nama file dengan id buku
		$filename = sprintf('%s.%0.8s', "2", $extension);
		
		$dir = $this->get('settings')['upload_directory'];

		$directory = $dir."/backup/".$dml["nama_project"];
		$nama_file = $dml["nama_file"];
		$path_cloud = $dml["nama_project"]."/".$nama_file;
		if (!file_exists($directory)) {
			mkdir($directory, 0777, true);
		}
		$uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $nama_file);
	   
	    $nama_project = $dml['nama_project'];
	    $nama_db = $dml['nama_db'];
	    $tanggal = $dml['tanggal'];
	    $tipe = $dml['tipe'];
	    $path = $dml['path'];
	    
		$query = $this->db->prepare("insert into histori_backup(nama_project, nama_db, tanggal, tipe, path, path_cloud) values(:nama_project, :nama_db, :tanggal, :tipe, :path, :path_cloud)");
		$query->bindParam(':nama_project', $nama_project);  
		$query->bindParam(':nama_db', $nama_db);  
		$query->bindParam(':tanggal', $tanggal);  
		$query->bindParam(':tipe', $tipe);  
		$query->bindParam(':path', $path);  
		$query->bindParam(':path_cloud', $path_cloud);  
		if($query->execute()){
		   	return $response->withJson(["status" => "success", "data" => "1"], 200);    	
		}
	}
});

$app->post('/histori_dml/delete', function (Request $request, Response $response) {
    $dml = $request->getParsedBody();

    $nama_project = $dml['nama_project'];
    $jumlah_hari = $dml['jumlah_hari'];

	$query = $this->db->prepare("delete from histori_backup where nama_project = :nama_project and DATE(tanggal) <= DATE(NOW())- INTERVAL $jumlah_hari DAY");
	$query->bindParam(':nama_project', $nama_project);
	if($query->execute()){
	   	return $response->withJson(["status" => "success", "data" => "1"], 200);    	
	}
});

$app->get('/histori_dml/test', function (Request $request, Response $response) {

	$query = $this->db->prepare("select count(*) from histori_backup");
	if($query->execute()){
	   	return $response->withJson(["status" => "success", "data" => "1"], 200);    	
	}
});


$app->get('/histori_dml/load', function (Request $request, Response $response) {
	$db = $this->db;
	$cari        = $request->getQueryParam("cari");
	$tgl_dari    = $request->getQueryParam("tgl_dari");
	$tgl_sampai  = $request->getQueryParam("tgl_sampai");
	$project_seq = $request->getQueryParam("project_seq");
	$orderBy  	 = $request->getQueryParam("order_by");
	$limit    	 = $request->getQueryParam("limit");
	$offset   	 = $request->getQueryParam("offset");

	$filter  = "";
	$filter2 = "";

	if (!empty($cari)){
		$filter .= " AND ((nama_project LIKE '%$cari%') OR (nama_db LIKE '%$cari%') OR (tanggal LIKE '%$cari%') OR (tipe LIKE '%$cari%') ".
				   " OR (path LIKE '%$cari%') OR (path_cloud LIKE '%$cari%')) ";
	}

	if (!empty($tgl_dari)){		
		$filter .= " AND DATE(tanggal) >=  DATE('$tgl_dari') ";
	}

	if (!empty($tgl_sampai)){		
		$filter .= " AND DATE(tanggal) <=  DATE('$tgl_sampai') ";
	}

	if (!empty($project_seq)){
		if ($project_seq > 0){
			$filter .= " AND project_seq = $project_seq ";
		}
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

	$data = [];	
	try {

		$sql = 	"SELECT seq, nama_project, nama_db, tanggal, tipe, path, IFNULL(project_seq,0) AS project_seq, IFNULL(path_cloud,'') AS path_cloud ".
                "FROM histori_backup ".
				"WHERE seq <> 0 $filter $filter2 ";

		$query  = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil  = [];		
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil); 
});

$app->get('/histori_dml/load_gagal', function (Request $request, Response $response) {
	$db = $this->db;
	$cari        = $request->getQueryParam("cari");
	// $tanggal     = $request->getQueryParam("tanggal");	
	$project_seq = $request->getQueryParam("project_seq");
	$orderBy  	 = $request->getQueryParam("order_by");
	$limit    	 = $request->getQueryParam("limit");
	$offset   	 = $request->getQueryParam("offset");

	$filter  = "";
	$filter2 = "";

	if (!empty($cari)){
		$filter .= " AND ((p.nama LIKE '%$cari%') OR (h.tanggal LIKE '%$cari%')) ";				   
	}

	// if (!empty($tanggal)){		
	// 	$filter .= " AND DATE(tanggal) =  DATE('$tanggal') ";
	// }	

	if (!empty($project_seq)){
		if ($project_seq > 0){
			$filter .= " AND project_seq = $project_seq ";
		}
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

	$data = [];	
	try {
		$sql = 	"SELECT p.seq, p.kode, p.nama, p.disable_date, p.batas_backup, p.keterangan, MAX(h.tanggal) AS tgl_terakhir ".
				"FROM master_project p, histori_backup h ".
				"WHERE p.seq = h.project_seq AND DATEDIFF(NOW(), DATE(h.tanggal)) > p.batas_backup $filter ".
				"GROUP BY p.seq, p.kode, p.nama, p.disable_date, p.batas_backup, p.keterangan  $filter2 ";				
		$query  = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil  = [];		
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil); 
});


$app->get('/histori_dml/get_count_gagal', function (Request $request, Response $response) {
	$db = $this->db;

	$data = [];	
	try {
		$sql = 	"SELECT COUNT(*) AS jumlah FROM (".
					"SELECT COUNT(p.seq) ".
					"FROM master_project p, histori_backup h ".
					"WHERE p.seq = h.project_seq AND DATEDIFF(NOW(), DATE(h.tanggal)) > p.batas_backup ".
					"GROUP BY p.seq ".
				") AS data";
		$query  = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil  = [];		
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil); 
});

$app->get('/histori_dml/get_count_berhasil', function (Request $request, Response $response) {
	$db = $this->db;
	$data = [];	
	try {
		$sql = 	"SELECT COUNT(*) AS jumlah FROM (".
					"SELECT COUNT(p.seq) ".
					"FROM master_project p, histori_backup h ".
					"WHERE p.seq = h.project_seq AND DATE(h.tanggal) = DATE(NOW()) ".
					"GROUP BY p.seq ".
				") AS data";
		$query  = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil  = [];		
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil); 
});