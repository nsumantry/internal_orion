<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

require __DIR__ . '/../src/lain_lain/api.php';
require __DIR__ . '/../src/lain_lain/histori_dml.php';
require __DIR__ . '/../src/lain_lain/global.php';
require __DIR__ . '/../src/system_user/system_user.php';
require __DIR__ . '/../src/lain_lain/master_project.php';

?>