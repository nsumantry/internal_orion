<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';
require_once __DIR__ . '/../../src/general/engine_general.php';



$app->get('/system_user/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
	$seq = $request->getQueryParam("seq");

	try {
        $sql = 	"SELECT seq, user_id, nama, is_tambah_user, tgl_non_aktif ".
                "FROM system_user ".
                "WHERE seq = $seq";
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});



$app->get('/system_user/masuk', function (Request $request, Response $response, array $args) {
    $db = $this->db;	
    
    $user_id       = $request->getQueryParam("user_id");
	$paskata_sandi = $request->getQueryParam("kata_sandi");
	$seq = 0;

    if (empty($user_id) && ($user_id == "")){ 
        return $response->withJson(setInfo(STATUS_GAGAL, "User id belum diisi", $seq), 200);  
    }
    
    if (empty($paskata_sandi) && ($paskata_sandi == "")){ 
        return $response->withJson(setInfo(STATUS_GAGAL, "kata sandi belum diisi", $seq), 200);  
	}
    
	try {
        if (get_count($db, "system_user","UPPER(user_id) = UPPER('$user_id') AND tgl_non_aktif IS NULL") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Used id belum terdaftar", $seq), 200);
        }

		$sql = 	"SELECT seq, user_id, kata_sandi ".
                "FROM system_user ".
                "WHERE (UPPER(user_id) = UPPER('$user_id')) AND kata_sandi = '$paskata_sandi' AND tgl_non_aktif IS NULL";                
		$query = $db->prepare($sql); 
        $query->execute();
        $rowCount = $query->rowCount();
        if ($rowCount > 0){                  
            $hasil = [];
            $data = $query->fetchAll();
            $hasil = setHasil(STATUS_SUKSES, $data);
            return $response->withJson($hasil);
        }else{
            return $response->withJson(setInfo(STATUS_GAGAL, "Used id atau kata sandi salah",$seq), 200);
        }
	} catch(PDOException $pdoe) {
		return $response->withJson(setInfo(PESAN_GAGAL_KESALAHAN, "", $seq), 200);
	}
});


$app->post('/system_user/ubah_password', function (Request $request, Response $response) {	
	$db = $this->db;    
	$seq = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $seq              = $dtPost['seq'];        
        $kata_sandi_lama  = $dtPost['kata_sandi_lama'];
        $kata_sandi_baru  = $dtPost['kata_sandi_baru'];        
        
        if (empty($pswd_lama) && ($kata_sandi_lama == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password lama belum diisi", $seq), 200);
        }

        if (empty($pswd_baru) && ($kata_sandi_baru == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password baru belum diisi", $seq), 200);
        }

        if (get_count($db, "system_user","kata_sandi = '$kata_sandi_lama' AND seq = $seq") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Password lama tidak sesuai", $seq), 200);
        }
		
		$sql =  "UPDATE system_user SET ".
					"kata_sandi = :kata_sandi ".					
				"WHERE seq = :seq";
		$query = $db->prepare($sql);
		$query->bindParam(':seq', $seq);
		$query->bindParam(':kata_sandi', $kata_sandi_baru);
        $result = $query->execute();        
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $seq), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $seq), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $seq), 200);   
});