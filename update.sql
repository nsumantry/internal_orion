CREATE TABLE system_user(
  seq INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id VARCHAR(100),
  nama VARCHAR(100),
  kata_sandi VARCHAR(100),
  is_tambah_user VARCHAR(1),
  tgl_non_aktif DATE
)
